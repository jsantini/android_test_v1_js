package com.jsantini.testandroid;

import android.os.SystemClock;
import android.support.test.espresso.IdlingRegistry;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.RecyclerView;

import com.jsantini.testandroid.ui.repositories.RepositoriesActivity;
import com.jsantini.testandroid.util.EspressoIdlingResource;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Random;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by jsantini on 05/03/18.
 */

@RunWith(AndroidJUnit4.class)
@LargeTest
public class NavigationTest {

    @Rule
    public ActivityTestRule<RepositoriesActivity> mActivityRule =
            new ActivityTestRule<>(RepositoriesActivity.class);

    @Before
    public void registerIdlingResource() {
        IdlingRegistry.getInstance().register(EspressoIdlingResource.getIdlingResource());
    }

    @After
    public void unregisterIdlingResource() {
        IdlingRegistry.getInstance().unregister(EspressoIdlingResource.getIdlingResource());
    }

    @Test
    public void navigationBetweenViewsTest() {
        // Check that repositories Activity was opened.
        onView(withId(R.id.rv_repositories)).check(matches(isDisplayed()));

        int x = getRandomRecyclerPosition(R.id.rv_repositories);

        onView(withId(R.id.rv_repositories))
                .perform(RecyclerViewActions
                        .actionOnItemAtPosition(x, click()));

        //Wait for view
        SystemClock.sleep(1500);

        onView(withId(R.id.rv_pull_requests)).check(matches(isDisplayed()));

        pressBack();

        onView(withId(R.id.rv_repositories)).check(matches(isDisplayed()));
    }

    private int getRandomRecyclerPosition(int recyclerId) {
        Random ran = new Random();
        RecyclerView recyclerView = (RecyclerView) mActivityRule
                .getActivity().findViewById(recyclerId);

        int n = (recyclerView == null)
                ? 1
                : recyclerView.getAdapter().getItemCount();

        return ran.nextInt(n);
    }
}

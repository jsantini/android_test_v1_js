package com.jsantini.testandroid.ui.pullRequests;

import com.jsantini.testandroid.network.PullRequestNetwork;

import dagger.Module;
import dagger.Provides;

/**
 * Created by jean.santini on 05/03/2018.
 */

@Module
public class PullRequestsModule {
    private PullRequestsContract.View view;
    private PullRequestNetwork pullRequestNetwork;

    public PullRequestsModule(PullRequestsContract.View view, PullRequestNetwork pullRequestNetwork) {
        this.view = view;
        this.pullRequestNetwork = pullRequestNetwork;
    }

    @Provides
    public PullRequestsContract.View provideView() {
        return view;
    }

    @Provides
    public PullRequestsContract.Presenter providePresenter(PullRequestsContract.View pullRequestsView, PullRequestNetwork pullRequestNetwork) {
        return new PullRequestsPresenter(pullRequestsView, pullRequestNetwork);
    }

}

package com.jsantini.testandroid.network;

import android.content.Context;
import android.support.annotation.NonNull;

import com.jsantini.testandroid.model.Repository;
import com.jsantini.testandroid.network.dto.ApiGenericResponseDTO;
import com.jsantini.testandroid.util.NetworkUtils;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by jsantini on 04/03/18.
 */

public class RepositoryNetwork {

    @Inject
    Context mContext;

    @Inject
    GithubAPI.GithubAPIContract service;

    @Inject
    public RepositoryNetwork() {
    }

    public void getRepositories(final int page, final NetworkCallback.DefaultCallback repositoriesCallback) {
        if(NetworkUtils.isConnected(mContext)) {
            Call<ApiGenericResponseDTO<List<Repository>>> call = service.getRepositories(page);

            call.enqueue(new Callback<ApiGenericResponseDTO<List<Repository>>>() {
                @Override
                public void onResponse(Call<ApiGenericResponseDTO<List<Repository>>> call, Response<ApiGenericResponseDTO<List<Repository>>> response) {
                    if(response.isSuccessful()) {
                        repositoriesCallback.onSuccess(response);
                    } else {
                        repositoriesCallback.onError();
                    }
                }

                @Override
                public void onFailure(Call<ApiGenericResponseDTO<List<Repository>>> call, Throwable t) {
                    repositoriesCallback.onGenericError();
                }
            });
        } else {
            repositoriesCallback.onConnectionError();
        }
    }
}

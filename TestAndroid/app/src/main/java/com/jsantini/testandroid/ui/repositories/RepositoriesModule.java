package com.jsantini.testandroid.ui.repositories;

import com.jsantini.testandroid.di.ActivityScoped;
import com.jsantini.testandroid.network.RepositoryNetwork;

import javax.annotation.Nullable;
import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

/**
 * Created by jsantini on 04/03/18.
 */

@Module
public class RepositoriesModule {

    private RepositoriesContract.View view;
    private RepositoryNetwork repositoryNetwork;

    public RepositoriesModule(RepositoriesContract.View view, RepositoryNetwork repositoryNetwork) {
        this.view = view;
        this.repositoryNetwork = repositoryNetwork;
    }

    @Provides public RepositoriesContract.View provideView() {
        return view;
    }

    @Provides
    public RepositoriesContract.Presenter providePresenter(RepositoriesContract.View repositoriesView, RepositoryNetwork repositoryNetwork) {
        return new RepositoriesPresenter(repositoriesView, repositoryNetwork);
    }

}

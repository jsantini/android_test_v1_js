package com.jsantini.testandroid.ui;

/**
 * Created by jsantini on 04/03/18.
 */

public interface BasePresenter {

    void start();

}

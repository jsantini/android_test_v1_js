package com.jsantini.testandroid.network;

/**
 * Created by jsantini on 04/03/18.
 */

import com.jsantini.testandroid.model.PullRequest;
import com.jsantini.testandroid.model.Repository;
import com.jsantini.testandroid.network.dto.ApiGenericResponseDTO;

import java.util.List;

import retrofit2.Response;

public abstract class NetworkCallback {

    public interface DefaultCallback<R> {
        void onSuccess(Response response);

        void onError();

        void onGenericError();

        void onConnectionError();
    }
}

package com.jsantini.testandroid.di;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

/**
 * Created by jsantini on 04/03/18.
 */

@Module
public class ApplicationModule {

    Application mApplication;

    Context mContext;

    public ApplicationModule(Application application, Context context) {
        mApplication = application;
        mContext = context;
    }

    @Provides
    @Singleton
    Application providesApplication() {
        return mApplication;
    }

    @Provides
    @Singleton
    Context providesContext() {
        return mContext;
    }

}

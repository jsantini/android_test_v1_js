package com.jsantini.testandroid.di;

import com.jsantini.testandroid.ui.pullRequests.PullRequestsActivity;
import com.jsantini.testandroid.ui.pullRequests.PullRequestsModule;

import dagger.Component;

/**
 * Created by jean.santini on 05/03/2018.
 */

@ActivityScoped @Component(dependencies = AppComponent.class, modules = PullRequestsModule.class)
public interface PullRequestsComponent {

    void inject(PullRequestsActivity pullRequestsActivity);
}

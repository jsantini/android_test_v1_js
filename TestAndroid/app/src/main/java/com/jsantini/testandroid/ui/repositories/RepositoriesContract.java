package com.jsantini.testandroid.ui.repositories;

import com.jsantini.testandroid.model.Repository;
import com.jsantini.testandroid.ui.BasePresenter;
import com.jsantini.testandroid.ui.BaseView;

import java.util.List;

/**
 * Created by jsantini on 04/03/18.
 */

public interface RepositoriesContract {

    interface View extends BaseView<Presenter> {


        String getMsgLoadingRepositories();

        void showRepositories(List<Repository> repositories, long totalItems);

        void showLoadRepositoriesError();
    }

    interface Presenter extends BasePresenter {

        void loadRepositories(int page);

        int getPage();

        List<Repository> getItems();

        void setSavedState(int page, long totalItems, List<Repository> repositories);

        long getTotalItems();
    }
}

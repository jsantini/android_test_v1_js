package com.jsantini.testandroid.di;

import com.jsantini.testandroid.ui.repositories.RepositoriesActivity;
import com.jsantini.testandroid.ui.repositories.RepositoriesModule;

import dagger.Component;

/**
 * Created by jean.santini on 05/03/2018.
 */

@ActivityScoped @Component(dependencies = AppComponent.class, modules = RepositoriesModule.class)
public interface RepositoriesComponent {

    void inject(RepositoriesActivity repositoriesActivity);

}

package com.jsantini.testandroid.application;

import android.app.Application;

import com.jsantini.testandroid.di.AppComponent;
import com.jsantini.testandroid.di.ApplicationModule;
import com.jsantini.testandroid.di.DaggerAppComponent;
import com.jsantini.testandroid.network.NetworkModule;

/**
 * Created by jsantini on 04/03/18.
 */

public class TestAndroidApplication extends Application {

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = DaggerAppComponent.builder()
                .applicationModule(new ApplicationModule(this, this))
                .networkModule(new NetworkModule("https://api.github.com"))
                .build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

}

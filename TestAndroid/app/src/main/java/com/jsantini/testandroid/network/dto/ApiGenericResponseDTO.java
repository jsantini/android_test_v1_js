package com.jsantini.testandroid.network.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by jsantini on 04/03/18.
 */

public class ApiGenericResponseDTO<T> {

    @SerializedName("total_count")
    @Expose
    private long totalCount;

    @SerializedName("incomplete_results")
    @Expose
    private boolean incompleteResults;

    @Expose
    private T items;

    public long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }

    public boolean isIncompleteResults() {
        return incompleteResults;
    }

    public void setIncompleteResults(boolean incompleteResults) {
        this.incompleteResults = incompleteResults;
    }

    public T getItems() {
        return items;
    }

    public void setItems(T items) {
        this.items = items;
    }
}

package com.jsantini.testandroid.ui.pullRequests;

import android.support.annotation.NonNull;

import com.jsantini.testandroid.model.PullRequest;
import com.jsantini.testandroid.network.NetworkCallback;
import com.jsantini.testandroid.network.PullRequestNetwork;
import com.jsantini.testandroid.ui.BaseView;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Response;

/**
 * Created by jean.santini on 05/03/2018.
 */

public class PullRequestsPresenter implements PullRequestsContract.Presenter {

    private final PullRequestsContract.View mPullRequestView;
    private final PullRequestNetwork mPullRequestNetwork;
    private String creator;
    private String repository;
    private List<PullRequest> pullRequests;

    @Inject
    public PullRequestsPresenter(PullRequestsContract.View view,
                                 @NonNull final PullRequestNetwork pullRequestNetwork) {

        this.mPullRequestView = view;
        this.mPullRequestNetwork = pullRequestNetwork;
    }

    @Override
    public void start() {
        if(pullRequests != null) {
            mPullRequestView.showOrHideLoading(false, null);
            mPullRequestView.showPullRequests(pullRequests);
        } else {
            loadPullRequests();
        }
    }

    private void loadPullRequests() {
        mPullRequestView.showOrHideLoading(true, mPullRequestView.getMsgLoadingPullRequests());
        mPullRequestNetwork.getPullRequests(creator, repository, new NetworkCallback.DefaultCallback() {
            @Override
            public void onSuccess(Response response) {
                pullRequests = (List<PullRequest>) response.body();
                Collections.sort(pullRequests, Collections.reverseOrder(new Comparator<PullRequest>() {
                    @Override
                    public int compare(PullRequest pr1, PullRequest pr2) {
                        return pr1.getUpdatedAt().compareTo(pr2.getUpdatedAt());
                    }
                }));

                if(mPullRequestView.isActive()) {
                    mPullRequestView.showOrHideLoading(false, null);
                    mPullRequestView.showPullRequests(pullRequests);
                }

            }

            @Override
            public void onError() {
                if(mPullRequestView.isActive()) {
                    mPullRequestView.showOrHideLoading(false, null);
                    mPullRequestView.showLoadPullRequestsError();
                }
            }

            @Override
            public void onGenericError() {
                if(mPullRequestView.isActive()) {
                    mPullRequestView.showGenericError();
                }
            }

            @Override
            public void onConnectionError() {
                if(mPullRequestView.isActive()) {
                    mPullRequestView.showConnectionError();
                }
            }
        });
    }

    @Override
    public List<PullRequest> getItems() {
        return pullRequests;
    }

    @Override
    public void setSavedState(List<PullRequest> pullRequests) {
        this.pullRequests = pullRequests;
    }

    @Override
    public void setParameters(String creator, String repository) {
        this.creator = creator;
        this.repository = repository;
    }
}

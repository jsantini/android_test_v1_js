package com.jsantini.testandroid.ui.repositories;

import com.jsantini.testandroid.model.Repository;
import com.jsantini.testandroid.network.NetworkCallback;
import com.jsantini.testandroid.network.RepositoryNetwork;
import com.jsantini.testandroid.network.dto.ApiGenericResponseDTO;
import com.jsantini.testandroid.util.EspressoIdlingResource;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Response;

/**
 * Created by jsantini on 04/03/18.
 */

final class RepositoriesPresenter implements RepositoriesContract.Presenter {

    private RepositoriesContract.View mRepositoriesView;
    RepositoryNetwork mRepositoriesNetwork;
    private int page = 1;
    private long totalItems;
    private List<Repository> repositories;

    @Inject
    RepositoriesPresenter(RepositoriesContract.View view, RepositoryNetwork repositoryNetwork) {
        mRepositoriesNetwork = repositoryNetwork;
        mRepositoriesView = view;
    }

    @Override
    public void start() {
        if(repositories != null) {
            mRepositoriesView.showOrHideLoading(false, null);
            mRepositoriesView.showRepositories(repositories, totalItems);
        } else {
            loadRepositories(page);
        }
    }

    @Override
    public void loadRepositories(final int page) {
        this.page = page;
        mRepositoriesView.showOrHideLoading(true, mRepositoriesView.getMsgLoadingRepositories());
        EspressoIdlingResource.increment();

        mRepositoriesNetwork.getRepositories(page, new NetworkCallback.DefaultCallback() {
            @Override
            public void onSuccess(Response response) {
                ApiGenericResponseDTO<List<Repository>> apiResponse = (ApiGenericResponseDTO<List<Repository>>) response.body();
                repositories = apiResponse.getItems();
                totalItems = apiResponse.getTotalCount();
                if(mRepositoriesView.isActive()) {
                    mRepositoriesView.showOrHideLoading(false, null);
                    mRepositoriesView.showRepositories(repositories, totalItems);
                }
                if (!EspressoIdlingResource.getIdlingResource().isIdleNow()) {
                    EspressoIdlingResource.decrement();
                }
            }

            @Override
            public void onError() {
                if(mRepositoriesView.isActive()) {
                    mRepositoriesView.showLoadRepositoriesError();
                }
            }

            @Override
            public void onGenericError() {
                if(mRepositoriesView.isActive()) {
                    mRepositoriesView.showGenericError();
                }
            }

            @Override
            public void onConnectionError() {
                if(mRepositoriesView.isActive()) {
                    mRepositoriesView.showConnectionError();
                }
            }
        });
    }

    @Override
    public int getPage() {
        return page;
    }

    @Override
    public List<Repository> getItems() {
        return repositories;
    }

    @Override
    public void setSavedState(final int page, final long totalItems, final List<Repository> repositories) {
        this.page = page;
        this.totalItems = totalItems;
        this.repositories = repositories;
    }

    @Override
    public long getTotalItems() {
        return totalItems;
    }
}

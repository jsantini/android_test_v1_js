package com.jsantini.testandroid.network;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jsantini.testandroid.model.PullRequest;
import com.jsantini.testandroid.model.Repository;
import com.jsantini.testandroid.network.dto.ApiGenericResponseDTO;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by jsantini on 04/03/18.
 */

public class GithubAPI {

    public interface GithubAPIContract {
        @GET("search/repositories?q=language:Java&sort=stars&page=1")
        Call<ApiGenericResponseDTO<List<Repository>>> getRepositories(@Query("page") int page);

        @GET("repos/{creator}/{repository}/pulls")
        Call<List<PullRequest>> getPullRequests(@Path("creator") String creator,
                                                @Path("repository") String repository);
    }
}

package com.jsantini.testandroid.network;

import android.content.Context;
import android.support.annotation.NonNull;

import com.jsantini.testandroid.model.PullRequest;
import com.jsantini.testandroid.util.NetworkUtils;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by jean.santini on 05/03/2018.
 */

public class PullRequestNetwork {

    @Inject
    Context mContext;

    @Inject
    GithubAPI.GithubAPIContract service;

    @Inject
    public PullRequestNetwork() {
    }

    public void getPullRequests(@NonNull final String creator,
                                @NonNull final String repository,
                                @NonNull final NetworkCallback.DefaultCallback networkCallback) {
        if(NetworkUtils.isConnected(mContext)) {

            Call<List<PullRequest>> call = service.getPullRequests(creator, repository);

            call.enqueue(new Callback<List<PullRequest>>() {
                @Override
                public void onResponse(Call<List<PullRequest>> call, Response<List<PullRequest>> response) {
                    if(response.isSuccessful()) {
                        networkCallback.onSuccess(response);
                    } else {
                        networkCallback.onError();
                    }
                }

                @Override
                public void onFailure(Call<List<PullRequest>> call, Throwable t) {
                    networkCallback.onGenericError();
                }
            });
        } else {
            networkCallback.onConnectionError();
        }
    }


}

package com.jsantini.testandroid.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by jsantini on 04/03/18.
 */

public class Author implements Serializable {

    @SerializedName("login")
    @Expose
    private String authorName;

    @SerializedName("avatar_url")
    @Expose
    private String authorAvatar;

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getAuthorAvatar() {
        return authorAvatar;
    }

    public void setAuthorAvatar(String authorAvatar) {
        this.authorAvatar = authorAvatar;
    }
}

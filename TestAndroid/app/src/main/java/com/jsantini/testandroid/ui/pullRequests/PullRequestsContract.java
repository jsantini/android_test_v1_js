package com.jsantini.testandroid.ui.pullRequests;

import com.jsantini.testandroid.model.PullRequest;
import com.jsantini.testandroid.ui.BasePresenter;
import com.jsantini.testandroid.ui.BaseView;

import java.util.List;

/**
 * Created by jean.santini on 05/03/2018.
 */

public interface PullRequestsContract {

    interface View extends BaseView<Presenter> {

        void showLoadPullRequestsError();

        void showPullRequests(List<PullRequest> pullRequests);

        String getMsgLoadingPullRequests();
    }

    interface Presenter extends BasePresenter {

        List<PullRequest> getItems();

        void setSavedState(List<PullRequest> pullRequests);

        void setParameters(String creator, String repository);
    }
}

package com.jsantini.testandroid.ui;

/**
 * Created by jsantini on 04/03/18.
 */

public interface BaseView<T> {

    void showOrHideLoading(boolean isShowLoading, String msg);

    void showGenericError();

    void showConnectionError();

    boolean isActive();
}

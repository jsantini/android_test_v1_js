package com.jsantini.testandroid.di;

import android.app.Application;
import android.content.Context;

import com.google.gson.Gson;
import com.jsantini.testandroid.application.TestAndroidApplication;
import com.jsantini.testandroid.network.GithubAPI;
import com.jsantini.testandroid.network.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;
import okhttp3.OkHttpClient;

/**
 * Created by jsantini on 04/03/18.
 */

@Singleton
@Component(modules = {
        ApplicationModule.class,
        NetworkModule.class}
    )
public interface AppComponent {

    void inject(TestAndroidApplication application);

    Application application();

    Gson gson();

    OkHttpClient okHttpClient();

    Context context();

    GithubAPI.GithubAPIContract githubAPIContract();
}

package com.jsantini.testandroid.ui.repositories;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.jsantini.testandroid.R;
import com.jsantini.testandroid.application.TestAndroidApplication;
import com.jsantini.testandroid.di.AppComponent;
import com.jsantini.testandroid.di.DaggerRepositoriesComponent;
import com.jsantini.testandroid.model.Repository;
import com.jsantini.testandroid.network.GithubAPI;
import com.jsantini.testandroid.network.RepositoryNetwork;
import com.jsantini.testandroid.ui.BaseActivity;
import com.jsantini.testandroid.ui.adapter.RepositoriesAdapter;
import com.jsantini.testandroid.ui.dialog.MessageDialog;
import com.jsantini.testandroid.ui.pullRequests.PullRequestsActivity;
import com.jsantini.testandroid.util.EndlessScrollListener;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RepositoriesActivity extends BaseActivity implements RepositoriesAdapter.OnRepositoryClickListener, RepositoriesContract.View {

    private static final String STATE_PAGE = "PAGE";
    private static final String STATE_ITEMS = "ITEMS";
    private static final String STATE_TOTAL_ITEMS = "TOTAL_ITEMS";

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.rv_repositories)
    RecyclerView rvRepositories;

    RepositoriesAdapter repositoriesAdapter;

    @Inject
    RepositoriesPresenter mPresenter;

    EndlessScrollListener mEndlessScrollListener;
    private boolean isSyncFinished;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repositories);

        ButterKnife.bind(this);
        setupComponent(((TestAndroidApplication) getApplication()).getAppComponent());
        setupToolbar();
        setupList();
        if (savedInstanceState != null) {
            mPresenter.setSavedState(savedInstanceState.getInt(STATE_PAGE),
                    savedInstanceState.getLong(STATE_TOTAL_ITEMS),
                    (List<Repository>) savedInstanceState.getSerializable(STATE_ITEMS));
        }
    }

    protected void setupComponent(AppComponent appComponent) {

        DaggerRepositoriesComponent.builder()
                .appComponent(appComponent)
                .repositoriesModule(new RepositoriesModule(this, new RepositoryNetwork()))
                .build()
                .inject(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.start();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong(STATE_TOTAL_ITEMS, mPresenter.getTotalItems());
        outState.putInt(STATE_PAGE, mPresenter.getPage());
        outState.putSerializable(STATE_ITEMS, (Serializable) mPresenter.getItems());
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        ab.setTitle(R.string.repositories_title);
        ab.setDisplayHomeAsUpEnabled(false);
    }

    private void setupList() {
        repositoriesAdapter = new RepositoriesAdapter(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rvRepositories.setLayoutManager(linearLayoutManager);
        rvRepositories.setAdapter(repositoriesAdapter);
        mEndlessScrollListener = new EndlessScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                repositoriesAdapter.setIsAppending(true);
                mPresenter.loadRepositories(current_page);
            }
        };
        rvRepositories.addOnScrollListener(mEndlessScrollListener);
        repositoriesAdapter.setmOnRepositoryClickListener(this);
    }

    @Override
    public void onRepositoryClick(@NonNull final String creator, @NonNull final String repository) {
        startActivity(PullRequestsActivity.getStartIntent(this, creator, repository));
    }

    @Override
    public String getMsgLoadingRepositories() {
        return getString(R.string.msg_loading_repositories);
    }

    @Override
    public void showRepositories(final List<Repository> repositories, final long totalItems) {
        mEndlessScrollListener.setTotalEntries(totalItems);
        repositoriesAdapter.setRepositories(repositories);
        isSyncFinished = true;
    }

    @Override
    public void showLoadRepositoriesError() {
        showOrHideLoading(false, null);
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        MessageDialog messageDialog = MessageDialog.newInstance(
                getResources().getString(R.string.msg_load_repositories_error), null, MessageDialog.TYPE_ERROR);
        messageDialog.setCancelable(false);
        messageDialog.show(ft, "dialog");
        isSyncFinished = true;
    }

    public boolean isSyncFinished() {
        return isSyncFinished;
    }
}

package com.jsantini.testandroid.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jsantini.testandroid.R;
import com.jsantini.testandroid.model.PullRequest;
import com.jsantini.testandroid.ui.viewHolder.PullRequestsViewHolder;
import com.jsantini.testandroid.util.CircleTransformUtil;
import com.jsantini.testandroid.util.Utils;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by jean.santini on 05/03/2018.
 */

public class PullRequestsAdapter extends RecyclerView.Adapter<PullRequestsViewHolder> {

    private final Context mContext;
    private List<PullRequest> pullRequestList;
    private OnPullRequestClick mOnPullRequestClick;

    public PullRequestsAdapter(@NonNull final Context context) {
        mContext = context;
    }

    @Override
    public PullRequestsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_pull_request, parent, false);
        return new PullRequestsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PullRequestsViewHolder holder, final int position) {
        final PullRequest pullRequest = pullRequestList.get(position);

        if(pullRequest.getAuthor() != null) {
            holder.tvItemAuthorName.setText(pullRequest.getAuthor().getAuthorName());
            Picasso.with(mContext)
                    .load(pullRequest.getAuthor().getAuthorAvatar())
                    .placeholder(R.drawable.noimage)
                    .transform(new CircleTransformUtil())
                    .into(holder.ivItemAuthorAvatar);
        }

        if(!Utils.stringIsEmpty(pullRequest.getTitle())) {
            holder.tvItemTitle.setText(pullRequest.getTitle());
        }

        if(!Utils.stringIsEmpty(pullRequest.getBody())) {
            String body = pullRequest.getBody();
            if(body.length() > 245) {
                body = body.substring(0,245) + "...";
            }
            holder.tvItemBody.setText(body);
        }

        if(pullRequest.getUpdatedAt() != null) {
            holder.tvItemDate.setText(Utils.formatDate(pullRequest.getUpdatedAt()));
        }

        if(mOnPullRequestClick != null) {
            holder.llContentItemPullRequest.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mOnPullRequestClick.onPullRequestClick(pullRequest.getHtmlURL());
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        if(pullRequestList == null) {
            return 0;
        }
        return pullRequestList.size();
    }

    public void setPullRequestList(final List<PullRequest> pullRequestList) {
        this.pullRequestList = pullRequestList;
        notifyDataSetChanged();
    }

    public interface OnPullRequestClick {
        void onPullRequestClick(String url);
    }

    public void setmOnPullRequestClick(@NonNull final OnPullRequestClick onPullRequestClick) {
        this.mOnPullRequestClick = onPullRequestClick;
    }
}

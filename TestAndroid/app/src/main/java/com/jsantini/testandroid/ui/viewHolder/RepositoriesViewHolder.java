package com.jsantini.testandroid.ui.viewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jsantini.testandroid.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by jsantini on 04/03/18.
 */

public class RepositoriesViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.ll_content_item_repository)
    public LinearLayout llContentItemRepository;

    @BindView(R.id.tv_item_name)
    public TextView tvItemName;

    @BindView(R.id.tv_item_description)
    public TextView tvItemDescription;

    @BindView(R.id.iv_item_author_avatar)
    public ImageView ivItemAuthorAvatar;

    @BindView(R.id.tv_item_author_name)
    public TextView tvItemAuthorName;

    @BindView(R.id.tv_item_forks_count)
    public TextView tvItemForksCount;

    @BindView(R.id.tv_item_stars_count)
    public TextView tvItemStarsCount;

    public RepositoriesViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
